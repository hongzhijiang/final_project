'''
Author: Yuxuan Zhou
Date:11/26/18
Class: ISTA 131
Section Leader: Andrew Rickus
Collaborators: ShangQing LI, HongZhi Jiang, WeiTi Chen

Description:
<summary of this program>
'''

import pandas as pd, matplotlib.pyplot as plt
import numpy as np 
import csv 
from sklearn.linear_model import LinearRegression

def figure():
	data = "Figure.csv"
	file = pd.read_csv(data,index_col=0)

	year_list = list(range(2007,2017))
	all_list = list(file["All_field"])
	science_list = list(file["Science"])
	Computer_list = list(file["Computer"])
	Geoscience_list = list(file["Geosciences"])
	Meteorology_list = list(file["Meteorology"])

	# picture 1
	plt.subplot(2,2,1)
	plt.bar(year_list,science_list,edgecolor="black",color="Red")
	plt.plot(year_list,science_list,color="black")
	plt.xlabel("Year",fontsize = 15)
	plt.ylabel("Science",fontsize =15, fontweight= "bold")
	plt.title("Spend on Science 2007 to 2016")
	# this plot is to show the relationship between year and science
	plt.subplot(2,2,2)
	plt.bar(year_list,Computer_list,edgecolor="black")
	plt.plot(year_list,Computer_list,color="black")
	plt.xlabel("Year",fontsize = 15)
	plt.ylabel("Computer",fontsize =15, fontweight= "bold")
	plt.title("Spend on Computer 2007 to 2016")
	# this plot is to show the relationship between year and computer 
	plt.subplot(2,2,3)
	plt.bar(year_list,Geoscience_list,edgecolor="black",color="Yellow")
	plt.plot(year_list,Geoscience_list,color="black")
	plt.xlabel("Year",fontsize = 15)
	plt.ylabel("Geoscience",fontsize =15, fontweight= "bold")
	plt.title("Spend on Geoscience 2007 to 2016")
	# this plot is to show the relationship between year and geoscience
	plt.subplot(2,2,4)
	plt.bar(year_list,Meteorology_list,edgecolor="black",color="Green")
	plt.plot(year_list,Meteorology_list,color="black")
	plt.xlabel("Year",fontsize = 15)
	plt.ylabel("Meteorology",fontsize =15, fontweight= "bold")
	plt.title("Spend on Matplotlib 2007 to 2016")
	# this plot is to show the relationship between year and meterorology
	plt.tight_layout()
	plt.show()
	plt.close()
		# picture 2
	plt.subplot(2,1,1)
	X = np.array(all_list).reshape(-1,1)
	Y = np.array(science_list).reshape(-1,1)
	linear_regressor = LinearRegression() 
	linear_regressor.fit(X, Y) 
	Y_Predict = linear_regressor.predict(X)
	plt.scatter(X,Y)
	plt.plot(X,Y_Predict,color='black')
	plt.xlabel("Total Spend",fontsize = 15)
	plt.ylabel("Science Field",fontsize = 15)
	plt.title("2007 to 2016 Science Field Compare Total")
	# this scatter plot is use for compare the total number with science field, science field is a huge part
	plt.subplot(2,1,2)
	X = np.array(science_list).reshape(-1,1)
	Y = np.array(Computer_list).reshape(-1,1)
	linear_regressor = LinearRegression() 
	linear_regressor.fit(X, Y) 
	Y_Predict = linear_regressor.predict(X)
	plt.scatter(X,Y,color='Yellow')
	plt.plot(X,Y_Predict,color='Red')
	plt.xlabel("Science Field",fontsize = 15)
	plt.ylabel("Computer Science",fontsize = 15)
	plt.title("2007 to 2016 Computer Science Compare Science")
	# the scatter plot is use for compare the total number with computer science, computer science field is a huge part
	plt.tight_layout()
	plt.show()
	plt.close()
	# picture 3
	plt.plot(year_list,Computer_list,label="Computer")
	plt.plot(year_list,Geoscience_list,label="Geosciences")
	plt.plot(year_list,Meteorology_list,label="Meteorology")
	plt.xlabel("Year",fontsize = 15)
	plt.ylabel("Number",fontsize = 15, fontweight= "bold")
	plt.title("The MAIN science Field")
	plt.legend()
   
	plt.tight_layout()
	plt.show()


	