import unittest, json, numpy as np, pandas as pd, io
#from textblob import TextBlob
from statistics import mean
from statistics import stdev
import statistics
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt


def Weiti_fig1():
    df=pd.read_csv("table4_2014.csv")
    year_list=[]
    HullLosses_list=[]
    ratio=[]
    for i in df["Year"]:
        year_list.append(i)
    for b in df["Hull Losses"]:
        HullLosses_list.append(b)
    for c in df["Hull Losses per Million Aircraft Hours Flown"]:
        ratio.append(c)
    fig, ax1 = plt.subplots()
    ax1.set_ylabel('Aircraft Losses',
                   color="red", fontsize=15)
    xticks1=year_list
    bar_width = 0.8
    opacity = 1.0
    ax1.set_xlabel('Year')
    rects1 = plt.bar(year_list, HullLosses_list, bar_width, alpha=opacity,
                     color='b'
                     )
    x = np.arange(len(year_list))
    for a, b in zip(x, HullLosses_list):
        plt.text(0.0290000000001111*a+0.05, 0.1, str(b), horizontalalignment='center', verticalalignment='center',
                 transform=ax1.transAxes)
    ax2 = ax1.twinx()
    ax2.plot(year_list, ratio, c='y', marker='o')
    for a, b in zip(x, ratio):
        k = +0.02 * a + 0.05
        plt.text(0.0290000000001111 * a + 0.05,b+0.05, round(b,2), horizontalalignment='center', verticalalignment='center',
                 transform=ax1.transAxes)
    plt.ylabel('Aircraft Losses per Million Aircraft Hours Flown', color="red", fontsize=15)
    plt.tight_layout()
    plt.show()
def Weiti_fig2sum(str1,df):
    list_if = df[str1].str.replace(',', '')
    b = 0
    sum1 = 0
    for i in list_if[1:]:
        if (type(i) != float):
            b = b + 1
            sum1 = sum1 + int(i)
    list_if = list_if.fillna(sum1 / b)
    s = sum(int(i) for i in list_if)
    return s
def Weiti_fig2():
    df1=pd.read_csv("file05.csv")
    df = df1.copy()
    s3=Weiti_fig2sum("Institution funds", df)
    s4=Weiti_fig2sum("Business", df)
    s5=Weiti_fig2sum("Nonprofit organizations", df)
    s6=Weiti_fig2sum("All other sources", df)
    s1=sum([int(i) for i in list(df["Federal government"][1:].str.replace(',', ''))])
    s2 = sum([int(i) for i in list(df["State and local government"][1:].str.replace(',', ''))])
    s=s1+s2+s3+s4+s5+s6
    plt.figure(figsize=(12, 5))
    plt.title('University R&D Money Source',fontsize=20)
    labels = [u'Federal government', u'State and local government', u'Institution funds', u'Business', u'Nonprofit organizations', u'All other sources']
    sizes = [s1,s2,s3,s4,s5,s6]
    explode = (0.1, 0, 0,0,0,0)
    patches, l_text, p_text = plt.pie(sizes, labels=labels,explode=explode,
                                      labeldistance=1.1, autopct='%3.1f%%', shadow=True,
                                      startangle=90)
    for t in l_text:
        t.set_size = (30)
    for t in p_text:
        t.set_size = (20)
    plt.axis('equal')
    plt.legend(loc="upper left",bbox_to_anchor=(0.8,0.5))
    plt.show()
def Weiti_fig3list(df1,year):
    list1=[int(i)*0.001 for i in list(df1[str(year)][2:26].str.replace(',', ''))]
    list2 = [int(i)*0.001 for i in list(df1[str(year)][27:].str.replace(',', ''))]
    return list1,list2

def Weiti_fig3():
    df = pd.read_csv("table4_2014.csv")
    year_list = []
    HullLosses_list = []
    ratio = []
    for i in df["Year"]:
        year_list.append(i)
    for b in df["Hull Losses"]:
        HullLosses_list.append(b)

    X = np.array(year_list).reshape(-1, 1)  # values converts it into a numpy array
    Y = np.array(HullLosses_list).reshape(-1, 1)  # -1 means that calculate the dimension of rows, but have 1 column
    linear_regressor = LinearRegression()  # create object for the class
    linear_regressor.fit(X, Y)  # perform linear regression
    Y_pred = linear_regressor.predict(X)  # make predictions
    plt.scatter(X, Y)
    plt.plot(X, Y_pred, color='red')
    plt.title('Annual loss of aircraft')
    plt.xlabel('Year')
    plt.ylabel('Aircraft loss')
    plt.show()
#"table4_2014.csv"
# Weiti_fig3()
# Weiti_fig1()
# Weiti_fig2()
def name():
    print()
    print("Weiti Chen's work")