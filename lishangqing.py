import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


def get_data1():
    data = "pokemon_alopez247.csv"
    file = pd.read_csv(data)
    poke ={}
    type1 = []
    number = []
    per_height =[]
    table = []
    for i in range(len(file['Type_1'])):
        if file['Type_1'][i] not in poke:
            poke[file['Type_1'][i]] = []
            poke[file['Type_1'][i]].append(1)
            poke[file['Type_1'][i]].append(file['Height_m'][i])
        else:
            poke[file['Type_1'][i]][0] += 1 
            poke[file['Type_1'][i]][1] += file['Height_m'][i]
    for key in poke:
        type1.append(key)
        number.append(poke[key][0])
        per_height.append(round(poke[key][1]/poke[key][0],2))
    table.append(type1)
    table.append(number)
    table.append(per_height)
    return table

def get_data2():
    data = "pokemon_alopez247.csv"
    file = pd.read_csv(data)
    poke ={}
    for i in range(len(file['Type_1'])):
        if file['Type_1'][i] not in poke:
            poke[file['Type_1'][i]] = []
            poke[file['Type_1'][i]].append(file['Catch_Rate'][i])
        else:
            poke[file['Type_1'][i]].append(file['Catch_Rate'][i])
    return poke

def get_fig1():
    table = get_data1()
    fig, ax1 = plt.subplots()
    fig.patch.set_facecolor('#F0FFFF')
    ax1.set_ylabel('Number of each type',color="blue", fontsize=15)
    ax1.set_xlabel('type',color = "b",fontsize= 14)
    width = 1/1.5
    plt.bar(table[0], table[1], width, color=["green","red","#BBFFFF","#BCEE68","gray","#B452CD","yellow","#DAA520","#EED2EE","#F5DEB3","#EEAEEE","#EE9A00","#F0F0F0","#E0FFFF","#8470FF","#3A5FCD","#C1CDCD","#87CEFA"])
    plt.xticks(rotation='46')  
    ax2 = ax1.twinx()
    ax2.plot(table[0],table[2],'--o',color = "black")
    plt.ylabel('Average height',color="blue", fontsize=15)
    plt.title('Amount of Type and Average height of Pokemon',fontsize=15)
    x=[i for i in range(0,18)]
    for a,b in zip(x,table[2]):
        plt.text(a+0.1, b+0.02, '%.2f' % b, ha='center', va= 'bottom',fontsize=9)
    plt.show()

def get_fig2():
    poke = get_data2()
    type1 = [key for key in poke]
    each =[poke[key] for key in type1]
    fig, ax = plt.subplots()
    fig = plt.gcf()
    fig.patch.set_facecolor('#E0FFFF')

    ax.patch.set_facecolor('#FBFCFC')
    plt.boxplot(each,labels=type1,sym='.',whis = 1.5)
    plt.xlabel('Type of Pokemon',color="black", fontsize=15)
    plt.ylabel('Catch Rate',color="#34495E", fontsize=15)
    plt.title('Catch rate of each type',fontsize=18)
    plt.xticks(rotation='46')  
    plt.show()


def get_fig3():
    #third time
    file = pd.read_csv("pokemon_alopez247.csv")
    poke = {}
    for i in range(len(file['Total'])):
        if file['Total'][i] not in poke:
            poke[file['Total'][i]] = 1
        else:
            poke[file['Total'][i]] += 1
    total = [int(key) for key in poke]
    total.sort()
    amount = []
    for i in total:
        amount.append(poke[i])

    x = np.array(total).reshape(-1, 1)
    y = np.array(amount).reshape(-1, 1)
    linear_regressor = LinearRegression() 
    linear_regressor.fit(x, y)
    y_pred = linear_regressor.predict(x)
    fig, ax = plt.subplots()
    fig.patch.set_facecolor('#BBFFFF')
    plt.scatter(total, amount,c="r",alpha=0.7)
    plt.plot(x, y_pred, color='gold')
    plt.title('Species strength of Pokemon',fontsize=15)
    plt.xlabel('Number of Species strength',fontsize=15)
    plt.ylabel('Amount of every Species strength number',fontsize=15)
    plt.show()