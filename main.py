import pandas as pd
import numpy as np
from datetime import datetime
from scipy import stats
import pandas as pd, matplotlib.pyplot as plt, numpy as np

import Weiti
import csv
import Hongzhi
import lishangqing
import yuxuanzhou


def main():
    Weiti.Weiti_fig3()
    Weiti.Weiti_fig1()
    Weiti.Weiti_fig2()

    filename = 'data.csv'
    data = Hongzhi.read_data(filename)
    data = Hongzhi.preprocess(data)
    Hongzhi.draw(data)

    lishangqing.get_fig1()
    lishangqing.get_fig2()
    lishangqing.get_fig3()

    yuxuanzhou.figure()


if __name__ == "__main__":
    main()