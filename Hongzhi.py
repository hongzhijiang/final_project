import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import train_test_split

def read_data(filename):
    data = pd.read_csv(filename)

    print("data describe ...")
    print(data.describe())

    print("data info ...")
    print(data.info())
    return data


def preprocess(data: pd.DataFrame):
    return data


def draw_scatter_with_plot(X, y):
    # Draw the relationship between price and sqft_living, living room

    plt.scatter(X, y)
    clf = LinearRegression()
    # print(data['sqft_living'].values.reshape(-1, 1))
    clf.fit(X, y)
    y_pred_price = clf.predict(X)
    plt.plot(X, y_pred_price, color='g')


def draw(data: pd.DataFrame):

    # Picture 1
    plt.figure(1, figsize=(16, 20))

    plt.subplot(321)
    data['bedrooms'].value_counts().plot(kind='bar')
    plt.xlabel('bedrooms number', fontsize=14)
    plt.ylabel('House number', fontsize=14)

    plt.subplot(322)
    data['bathrooms'].value_counts().plot(kind='bar')
    plt.xlabel('bathrooms number', fontsize=14)
    plt.ylabel('House number', fontsize=14)

    plt.subplot(323)
    data['floors'].value_counts().plot(kind='bar')
    plt.xlabel('floors number', fontsize=14)
    plt.ylabel('House number', fontsize=14)

    plt.subplot(324)
    data['view'].value_counts().plot(kind='bar')
    plt.xlabel('view number', fontsize=14)
    plt.ylabel('House number', fontsize=14)

    plt.subplot(325)
    data['waterfront'].value_counts().plot(kind='bar')
    plt.xlabel('waterfront number', fontsize=14)
    plt.ylabel('House number', fontsize=14)

    plt.subplot(326)
    data['condition'].value_counts().plot(kind='bar')
    plt.xlabel('condition number', fontsize=14)
    plt.ylabel('House number', fontsize=14)
   

    plt.savefig('some_features.png')
    plt.show()

    plt.close()
    # Picture 2
    plt.figure(1, figsize=(16, 9))
    plt.subplot(211)
    draw_scatter_with_plot(data['sqft_living'].values.reshape(-1, 1), data['price'].values)

    minx, maxx, miny, maxy = plt.axis()
    plt.text(x=maxx*0.65, y=maxy*0.9, fontsize=14, s='Without cleaning extremes price values', va="top", ha="left", bbox=dict(boxstyle="round", ec=(0., 0, 0), fc=(1., 1, 1),))

    plt.xlabel('Sqft living area', fontsize=12)
    plt.ylabel('House Price', fontsize=12)

    plt.subplot(212)
    # clean Maxmium and Minium,
    four = data['price'].describe()
    Q1 = four['25%']
    Q3 = four['75%']
    IQR = Q3 - Q1

    # 10这个系数代表的是对极端值的忍耐程度，越大说明对极端值的忍耐越大，这里通过处理发现，取值10比较合适
    upper = Q3 + 10 * IQR
    lower = Q1 - 10 * IQR
    print(upper, lower)

    # remove extremes value with extremes price, and price must > 0
    valid_price = data[(data['price'] >= lower) & (data['price'] > 0) & (data['price'] <= upper)]['price']
    valid_values = data.iloc[valid_price.index.values]['sqft_living']

    draw_scatter_with_plot(valid_values.values.reshape(-1, 1), valid_price.values)

    plt.text(x=maxx*0.65, y=maxy*0.9, fontsize=14, s='Cleaning extremes price values', va="top", ha="left", bbox=dict(boxstyle="round", ec=(0., 0, 0), fc=(1., 1, 1),))
    plt.xlabel('Sqft living area', fontsize=12)
    plt.ylabel('House Price', fontsize=12)

    plt.ylim(miny, maxy)
    plt.xlim(minx, maxx)

    plt.savefig('sqft_living_with_price.png')
    plt.show()
    plt.close()

   
    # Picture 3
    plt.figure(1, figsize=(16, 9))

    plt.subplot(211)

    features = ['bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'waterfront', 'view',
                'condition', 'sqft_above', 'sqft_basement']
    X = data[features].values
    y = data['price'].values

    # random state 的选择，对多个随机种子测试后发现，对于当前数据集，42是一个合适的随机种子，random state 等于42时，分割的test data set分布最均匀
    # 最合适
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42, test_size=0.2)

    clf = LinearRegression()
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    plt.plot(y_train, y_train, color='g')
    plt.scatter(y_pred, y_test)

    minx, maxx, miny, maxy = plt.axis()
    plt.text(x=maxx*0.65, y=maxy*0.9, fontsize=14, s='Without cleaning extremes price values', va="top", ha="left", bbox=dict(boxstyle="round", ec=(0., 0, 0), fc=(1., 1, 1),))

    plt.xlabel('Prediction price with test data set', fontsize=14)
    plt.ylabel('True price value with test data set', fontsize=14)


    plt.subplot(212)
    # clean
    X_clean = data[(data['price'] >= lower) & (data['price'] > 0) & (data['price'] <= upper)][features]
    y_clean = data.loc[X_clean.index.values]['price']

    X_train, X_test, y_train, y_test = train_test_split(X_clean, y_clean, random_state=42, test_size=0.2)

    clf = LinearRegression()
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    plt.plot(y_train, y_train, color='g')
    plt.scatter(y_pred, y_test)

    plt.ylim(miny, maxy)
    plt.xlim(minx, maxx)

    plt.text(x=maxx*0.65, y=maxy*0.9, fontsize=14, s='Cleaning extremes price values', va="top", ha="left", bbox=dict(boxstyle="round", ec=(0., 0, 0), fc=(1., 1, 1),))

    plt.xlabel('Prediction price with test data set', fontsize=14)
    plt.ylabel('True price value with test data set', fontsize=14)

    plt.savefig('True_and_prediction.png')

    plt.show()
    plt.close()




